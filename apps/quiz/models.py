from django.db import models


# under development
# using simple models for the time being


class Quiz(models.Model):

	title = models.CharField(max_length=100)
	description = models.CharField(max_length=250, 
		null=True, blank=True)

	def __unicode__(self):
		return u"%s" % (self.title)


class Level(models.Model):

	quiz = models.ForeignKey(Quiz)
	title = models.CharField(max_length=100)
	level = models.IntegerField()

	def __unicode__(self):
		return u"[level %s] %s" % (self.quiz, self.level)


class Question(models.Model):

	level = models.ForeignKey(Level)
	content = models.TextField()

	def __unicode__(self):
		return u"[%s:%s] %s" % (self.level.quiz, self.level.level, self.content)


class Answer(models.Model):

	question = models.ForeignKey(Question)
	content = models.TextField()
	is_correct = models.BooleanField(default=False)

	def __unicode__(self):
		return u"[%s] %s" % (self.question.content, self.content)


