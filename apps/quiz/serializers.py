from rest_framework import serializers

from apps.quiz.models import Quiz, Level, Question, Answer


class QuizSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Quiz


class LevelSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Level


class QuestionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Question


class AnswerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Answer



