from django.contrib import admin

from .models import Quiz, Level, Question, Answer

admin.site.register(Quiz)
admin.site.register(Level)
admin.site.register(Question)
admin.site.register(Answer)
